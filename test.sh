#!/bin/bash

#Setup
THIS=$(pwd)
DIRECTORY=~/MyWebServer
PATH_TO_BIN=~/MyWebServer/bin
PATH_TO_HTML=~/MyWebServer/bin/se/lnu/http/resources/inner
PATH_TO_LOG=~/MyWebServer/logs/log.txt
CLASSPATH=se.lnu.http.HTTPServerConsole
PORT=9001
COUNT_SUCCESS=0
COUNT_FAIL=0

function Assert {
  DIFF=$(diff ./actual/$1.txt ./expected/$1.txt)
  if [ "$DIFF" ];then
    echo "Test failed"
    let COUNT_FAIL++
  else
    echo "Test passed"
    let COUNT_SUCCESS++
  fi
  rm ./actual/$1.txt
}

TESTCASE="2.0"
echo
echo "Test Case 2.0: Download the code and compile"
if [ ! -d "$DIRECTORY" ]; then
  cd $HOME
  git clone https://github.com/dntoll/MyWebServer.git
  cd $DIRECTORY/src 
  find . -name "*.java" | xargs javac -d ../bin > $THIS/actual/$TESTCASE.txt

  DIFF=$(diff $THIS/actual/$1.txt $THIS/expected/$1.txt)
  if [ "$DIFF" ];then
    echo "Test failed"
    echo "Could not compile code check so you have java jdk and git installed."
    exit
  else
    cd $THIS
    echo "Test passed"
  fi
else
   echo "Skipping this test because directory exists. So I guess the code is already compiled"
fi

TESTCASE="2.1"
echo
echo "Test Case 2.1: Test if server starts and prodcuses log"
java -cp $PATH_TO_BIN $CLASSPATH $PORT $PATH_TO_HTML > /dev/null 2>&1 &
sleep .5
kill $! 
if [ -f "$PATH_TO_LOG" ]; then
  echo "Test passed"
else
  echo "Test failed"
fi
sleep 1

TESTCASE="2.2"
echo
echo "Test Case 2.2: Test if server starts and get index.html"
java -cp $PATH_TO_BIN $CLASSPATH $PORT $PATH_TO_HTML > /dev/null 2>&1 &
sleep .5 
curl -s localhost:$PORT/ > ./actual/$TESTCASE.txt
sleep 1 

Assert $TESTCASE
kill $! 

TESTCASE="2.3.1"
echo
echo "Test Case 2.3.1: Test server on port already in use"
# Testing on port below 1000 (You need to be root to use that)
java -cp $PATH_TO_BIN $CLASSPATH 80 $PATH_TO_HTML > ./actual/$TESTCASE.txt 2>&1 &
sleep .5
Assert $TESTCASE

TESTCASE="2.3.2"
echo
echo "Test Case 2.3.2: Test server on port 0"
java -cp $PATH_TO_BIN $CLASSPATH 0 $PATH_TO_HTML > ./actual/$TESTCASE.txt 2>&1 &
sleep .5
Assert $TESTCASE

TESTCASE="2.3.3"
echo
echo "Test Case 2.3.3: Test server on port string"
java -cp $PATH_TO_BIN $CLASSPATH x $PATH_TO_HTML > ./actual/$TESTCASE.txt 2>&1 &
sleep .5
Assert $TESTCASE

TESTCASE="2.3.4"
echo
echo "Test Case 2.3.4: Test server on port 70000"
java -cp $PATH_TO_BIN $CLASSPATH 700000 $PATH_TO_HTML > ./actual/$TESTCASE.txt 2>&1 &
sleep .5
Assert $TESTCASE

sleep .5
TESTCASE="2.4"
echo
echo "Test Case 2.4: Test start server with folder with no access"
mkdir $PATH_TO_HTML/test
echo "<html>test</html>" > $PATH_TO_HTML/test/index.html
chmod 300 $PATH_TO_HTML/test
java -cp $PATH_TO_BIN $CLASSPATH $PORT $PATH_TO_HTML/test > ./actual/$TESTCASE.txt 2>&1 &

sleep 1 
kill $! 
Assert $TESTCASE

# Delete the folder again
chmod -R 766 $PATH_TO_HTML/test
rm -rf $PATH_TO_HTML/test
sleep .5

# Start server for next test-cases
java -cp $PATH_TO_BIN $CLASSPATH $PORT $PATH_TO_HTML > /dev/null 2>&1 &
sleep .5

TESTCASE="2.7.1"
echo
echo "Test Case 2.7.1: Test message 200 OK"
curl -is localhost:$PORT | head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.7.2"
echo
echo "Test Case 2.7.2: Test message 400 Bad Request"
curl -is localhost:$PORT -H "Empty;" | head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.7.3"
echo
echo "Test Case 2.7.3: Test message 403 FORBIDDEN"
echo "<html>test</html>" > $PATH_TO_HTML/test.html
chmod 000 $PATH_TO_HTML/test.html
curl -is localhost:$PORT/test.html | head -n 1 > ./actual/$TESTCASE.txt

chmod 666 $PATH_TO_HTML/test.html
rm $PATH_TO_HTML/test.html

Assert $TESTCASE

TESTCASE="2.7.4"
echo
echo "Test Case 2.7.4: Test message 404 Not Found"
curl -is localhost:$PORT/notFound.html | head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.8.1"
echo
echo "Test Case 2.8.1: Test response PUT"
curl -is -X PUT localhost:$PORT| head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.8.2"
echo
echo "Test Case 2.8.2: Test response DELETE"
curl -is -X DELETE localhost:$PORT| head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.8.3"
echo
echo "Test Case 2.8.3: Test response UPDATE"
curl -is -X UPDATE localhost:$PORT| head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.8.4"
echo
echo "Test Case 2.8.4: Test response TRACE"
curl -is -X TRACE localhost:$PORT| head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.8.5"
echo
echo "Test Case 2.8.4: Test response GET HEAD"
curl -is -I localhost:$PORT| head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE

TESTCASE="2.9"
echo
echo "Test Case 2.9: Test shared resource oustide of container"
curl -is --path-as-is localhost:$PORT/../secret.html | head -n 1 > ./actual/$TESTCASE.txt
Assert $TESTCASE
# Stop the server
kill $!

echo
echo
fail="Test failed: $COUNT_FAIL"
echo $fail 
pass="Test success: $COUNT_SUCCESS"
echo $pass